package main

import (
	"bytes"
	"os"
	"testing"
)

func Test_concatenateCertificates(t *testing.T) {
	result := concatenateCertificates([]byte{0x41, 0x42}, []byte{0x43, 0x44})
	if !bytes.Equal(result, []byte{0x43, 0x44, 0x0d, 0x0a, 0x41, 0x42}) {
		t.Errorf("certificate concatenation returned unexpected result: %v", result)
	}
}

func Test_prepareCertificates(t *testing.T) {
	tempDir := os.TempDir()
	dummyCaCert := tempDir + "/dummyCaCert.crt"
	dummyCert := tempDir + "/dummyCert.crt"

	_ = os.WriteFile(dummyCaCert, []byte{0x41, 0x41, 0x41, 0x41}, 0400)
	_ = os.WriteFile(dummyCert, []byte{0x42, 0x42, 0x42, 0x42}, 0400)
	concCert, err := prepareCertificates(tempDir, dummyCaCert, dummyCert)
	if err != nil {
		t.Errorf("failed to prepare concatenated certificate: %v", err)
	}
	result, err := os.ReadFile(concCert)
	if err != nil {
		t.Errorf("failed to read concatenated certificate: %v", err)
	}
	if !bytes.Equal(result, []byte{0x42, 0x42, 0x42, 0x42, 0x0d, 0x0a, 0x41, 0x41, 0x41, 0x41}) {
		t.Errorf("certificate concatenation returned unexpected result: %v", result)
	}
	_ = os.Remove(dummyCaCert)
	_ = os.Remove(dummyCert)
}
