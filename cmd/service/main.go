package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"os"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/zsoltimre/badger/internal/config"
	"gitlab.com/zsoltimre/badger/internal/middleware"
	"gitlab.com/zsoltimre/badger/internal/operation"
	"gitlab.com/zsoltimre/badger/internal/service/mongodb"
)

func setupLogger(debug bool) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logLevel := zerolog.InfoLevel
	if debug {
		logLevel = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(logLevel)
}

func main() {
	previousUmask := syscall.Umask(0o027)
	defer syscall.Umask(previousUmask)
	setupLogger(false)
	config, err := config.Get()
	if err != nil {
		log.Fatal().Msgf("failed to load configuration: %v", err)
	}

	mongo, err := mongodb.New(config.MongodbUrl)
	if err != nil {
		log.Fatal().Msgf("failed to connect to MongoDB: %v", err)
	}

	gin.DisableConsoleColor()
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(func(ctx *gin.Context) {
		ctx.Set("config", config)
		ctx.Set("mongodb", mongo)
		ctx.Next()
	})
	r.GET("/v1/healthcheck", operation.V1Healthcheck)
	r.GET("/v1/:projectId/:branchName/:key", operation.V1GetBadge)
	r.POST("/v1/:projectId/:branchName", middleware.Authorization(), operation.V1UpdateBadge)

	if err := startWebServer(r, config); err != nil {
		log.Fatal().Msg(err.Error())
	}
}

func startWebServer(r *gin.Engine, config *config.Config) error {
	serverAddress := fmt.Sprintf(
		"%s:%s",
		config.ListenAddress,
		config.ListenPort,
	)
	certPath, e := prepareCertificates(
		config.TemporaryDirectory,
		config.TlsCaCertPath,
		config.TlsCertPath,
	)
	if e != nil {
		return e
	}
	certPool := x509.NewCertPool()
	caCert, err := os.ReadFile(config.TlsCaCertPath)
	if err != nil {
		return err
	}
	if !certPool.AppendCertsFromPEM(caCert) {
		return fmt.Errorf("failed to add CA certificate to the certificate pool")
	}
	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS13,
		ClientAuth: tls.NoClientCert,
		ClientCAs:  certPool,
	}
	s := http.Server{
		Addr:              serverAddress,
		Handler:           r,
		TLSConfig:         tlsConfig,
		ReadHeaderTimeout: 3 * time.Second,
	}
	log.Info().Msgf("Web server started on %s", serverAddress)
	return s.ListenAndServeTLS(certPath, config.TlsKeyPath)
}

func prepareCertificates(tempPath string, caCertPath string, certPath string) (string, error) {
	caCert, err := os.ReadFile(caCertPath) // #nosec G304 -- coming from env variable
	if err != nil {
		return "", fmt.Errorf("failed to read CA certificate file: %v", err)
	}
	cert, err := os.ReadFile(certPath) // #nosec G304 -- coming from env variable
	if err != nil {
		return "", fmt.Errorf("failed to read certificate file: %v", err)
	}
	targetFile := fmt.Sprintf("%s/%s", tempPath, "service.crt")
	if err := os.WriteFile(targetFile, concatenateCertificates(caCert, cert), 0400); err != nil {
		return "", fmt.Errorf("failed to create concatenated certificate file: %v", err)
	}
	return targetFile, nil
}

func concatenateCertificates(caCert []byte, cert []byte) []byte {
	certContent := append(cert, 0x0d, 0x0a)
	return append(certContent, caCert...)
}
