FROM golang:1.22.3 AS builder

ENV DEBIAN_FRONTEND noninteractive
ENV CGO_ENABLED 0
ENV GOOS linux
# ENV GOARCH arm64

RUN apt-get -y update
RUN apt-get -yq upgrade

WORKDIR /build
COPY ./ ./
RUN go version
RUN go mod tidy
RUN go build -ldflags="-extldflags=-static" ./cmd/service
RUN chmod 755 /build/service

FROM scratch

WORKDIR /
COPY --from=builder /build/service /service
ENTRYPOINT [ "/service" ]
