# Badger

Badger enables you to create and access custom GitHub and GitLab project badges easily.

## 1. Configuration

The following environment variables are available to configure the service.

| Name                | Required | Default      | Description |
| ------------------- | -------- | ------------ | ----------- |
| LISTEN_ADDRESS      | No       | 0.0.0.0      | The address the service listens on. If not set, the default value will be used. |
| LISTEN_PORT         | No       | 8000         | The port the service listens on. If not set, the default value will be used. |
| POSTGRESQL_URL      | Yes      | N/A          | The PostgreSQL URL. For example: `postgres://username:password@localhost:5432/database_name`. |
| MONGODB_URL         | Yes      | N/A          | The MongoDB URL. For example: `mongodb://username:password@mongodb-svc.default.svc.cluster.local:27017/badges`. |
| TLS_CA_CERT_PATH    | No       | /tls/ca.crt  | The absolute path to the CA certificate file. If not set, the service will try to load it from the location of the default value. |
| TLS_CERT_PATH       | No       | /tls/tls.crt | The absolute path to the service's certificate file. If not set, the service will try to load it from the location of the default value. |
| TLS_KEY_PATH        | No       | /tls/tls.key | The absolute path to the private key file. If not set, the service will try to it from the location of the default value. |
| TEMPORARY_DIRECTORY | No       | /tmp         | Absolute path to a temporary directory the service can write files to. This directory is only used to create a file with the CA certificate and service certificate concatenated. |
| UPDATE_TOKEN        | Yes      | N/A          | An at least `32` bytes long random secret to be presented as a Bearer token when creating/updating badges. |

Please note that TLS is mandatory and cannot be disabled, thus make sure the `TLS_*` environment variables are set up properly.

## 2. Usage

### 2.1. Health Check

```bash
BADGER_ADDRESS="localhost"

curl -kv https://${BADGER_ADDRESS}/v1/healthcheck
```

### 2.2. Store Badge

Issue the below command to create a badge that has the string "coverage" on the left and the value "45%" on the right.

```bash
BADGER_ADDRESS="localhost"
PROJECT_ID="ffc23d78-0b6c-48da-a116-a212925989bc"
BRANCH_NAME="master"

curl -kv -X POST https://${BADGER_ADDRESS}/v1/${PROJECT_ID}/${BRANCH_NAME} \
    -H 'Content-Type: application/json' \
    --data '{"key": "coverage", "value": "45%", "status": "warning"}'
```

### 2.3. Get Badge

Issue the below command to fetch the badge as an SVG document.

```bash
BADGER_ADDRESS="localhost"
PROJECT_ID="ffc23d78-0b6c-48da-a116-a212925989bc"
BRANCH_NAME="master"

curl -kv https://${BADGER_ADDRESS}/v1/${PROJECT_ID}/${BRANCH_NAME}/coverage
```

You should get a badge the looks like the below.

![Example Badge 1](./docs/badge-example-1.png)

### 2.4. Update Badge

A badge can be easily updated using the same post request it was created with. For example, to update the previously created coverage badge to say the coverage is now "80%", issue the below command.

```bash
BADGER_ADDRESS="localhost"
PROJECT_ID="ffc23d78-0b6c-48da-a116-a212925989bc"
BRANCH_NAME="master"
UPDATE_TOKEN="AAAAAAAAAABBBBBBBBBBCCCCCCCCCCEE"

curl -kv -X POST https://${BADGER_ADDRESS}/v1/${PROJECT_ID}/${BRANCH_NAME} \
    -H "Authorization: Bearer ${UPDATE_TOKEN}" \
    -H "Content-Type: application/json" \
    --data '{"key": "testing", "value": "90%", "status": "warning"}'
```

If you fetch the badge this time, you will see it has been updated. You should get a badge the looks like the below.

![Example Badge 2](./docs/badge-example-2.png)


### 2.5. Delete Badge

Not currently supported
