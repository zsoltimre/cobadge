package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

func Authorization() gin.HandlerFunc {
	return func(c *gin.Context) {
		logger := log.With().
			Str("source", c.ClientIP()).
			Str("middleware", "authorization").
			Logger()

		token, err := getTokenFromHeaders(&c.Request.Header)
		if err != nil {
			logger.Warn().
				Uint("response_code", http.StatusInternalServerError).
				Msg(err.Error())
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		config, err := getConfigFromContext(c)
		if err != nil {
			logger.Error().
				Uint("response_code", http.StatusInternalServerError).
				Msg(err.Error())
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if config.UpdateToken != token {
			logger.Error().
				Uint("response_code", http.StatusUnauthorized).
				Msg("invalid update token")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		c.Next()
	}
}

func getTokenFromHeaders(headers *http.Header) (string, error) {
	authorizationHeader := headers.Get("Authorization")
	if len(authorizationHeader) == 0 {
		return "", fmt.Errorf("authorization header missing")
	}
	parts := strings.Split(authorizationHeader, " ")
	if len(parts) != 2 {
		return "", fmt.Errorf("invalid authorization header")
	}
	if parts[0] != "Bearer" {
		return "", fmt.Errorf("not a valid bearer token")
	}
	if len(parts[1]) < 32 {
		return "", fmt.Errorf("bearer token value is less than 32 bytes long")
	}
	return parts[1], nil
}
