package middleware

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAuthorization_getTokenFromHeaders(t *testing.T) {
	assert := assert.New(t)
	headers := http.Header{}
	_, err := getTokenFromHeaders(&headers)
	assert.EqualError(err, "authorization header missing")

	headers = http.Header{}
	headers.Add("Authorization", "invalid")
	_, err = getTokenFromHeaders(&headers)
	assert.EqualError(err, "invalid authorization header")

	headers = http.Header{}
	headers.Add("Authorization", "Basic deadbeef")
	_, err = getTokenFromHeaders(&headers)
	assert.EqualError(err, "not a valid bearer token")

	headers = http.Header{}
	headers.Add("Authorization", "Bearer deadbeef")
	_, err = getTokenFromHeaders(&headers)
	assert.EqualError(err, "bearer token value is less than 32 bytes long")

	headers = http.Header{}
	headers.Add("Authorization", "Bearer deadbeefdeadbeefdeadbeefdeadbeef")
	result, err := getTokenFromHeaders(&headers)
	assert.Nil(err)
	assert.Equal("deadbeefdeadbeefdeadbeefdeadbeef", result)
}
