package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/zsoltimre/badger/internal/config"
)

func getConfigFromContext(c *gin.Context) (*config.Config, error) {
	v, found := c.Get("config")
	if !found {
		return nil, fmt.Errorf("config not found in service context")
	}
	config, success := v.(*config.Config)
	if !success {
		return nil, fmt.Errorf("invalid config in service context")
	}
	return config, nil
}
