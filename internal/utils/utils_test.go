package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtils_GetDocumentId(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(
		"9e9e8f93720eb1a624d9c4dad6308c8b5a5b9908d99c4273e1941ee2df77ab79",
		GetDocumentId("master", "coverage"),
	)
}
