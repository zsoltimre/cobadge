package utils

import (
	"crypto/sha256"
	"fmt"
)

func GetDocumentId(branchName string, key string) string {
	id := fmt.Sprintf("%s::%s", branchName, key)
	return fmt.Sprintf("%x", sha256.Sum256([]byte(id)))
}
