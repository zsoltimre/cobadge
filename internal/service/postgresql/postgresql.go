package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/zsoltimre/badger/internal/config"
)

type Postgresql struct {
	connection *pgx.Conn
}

func New(config *config.Config) (*Postgresql, error) {
	conn, err := pgx.Connect(context.Background(), config.PostgresUrl)
	if err != nil {
		return nil, fmt.Errorf("unable to connect to PostgreSQL database: %v", err)
	}
	return &Postgresql{
		connection: conn,
	}, nil
}
