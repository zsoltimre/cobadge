package mongodb

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/zsoltimre/badger/internal/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Badge struct {
	Id     string `json:"id" bson:"_id"`
	Branch string `json:"branch" bson:"branch"`
	Key    string `json:"key" bson:"key"`
	Data   []byte `json:"data" bson:"data"`
}

type Database struct {
	url      string
	client   *mongo.Client
	database *mongo.Database
}

func New(mongoUrl string) (*Database, error) {
	client, err := connect(mongoUrl)
	if err != nil {
		return nil, err
	}
	return &Database{
		url:      mongoUrl,
		client:   client,
		database: client.Database("badges"),
	}, nil
}

func (d *Database) Connect() (*mongo.Client, error) {
	return connect(d.url)
}

func connect(mongoUrl string) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoUrl))
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (d *Database) GetBadge(projectId string, branchName string, key string) (*Badge, error) {
	collection := d.database.Collection(projectId)
	id := utils.GetDocumentId(branchName, key)
	result := Badge{}
	filter := bson.D{{Key: "_id", Value: id}}
	if err := collection.FindOne(context.Background(), filter).Decode(&result); err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, nil
		}
		return nil, err
	}
	return &result, nil
}

func (d *Database) UpdateBadge(projectId string, branchName string, key string, data []byte) error {
	collection := d.database.Collection(projectId)
	id := utils.GetDocumentId(branchName, key)
	filter := bson.M{"_id": id}
	update := bson.M{
		"$set": bson.M{
			"_id":    id,
			"branch": branchName,
			"key":    key,
			"data":   data,
		},
	}
	opts := options.Update().SetUpsert(true)
	if _, err := collection.UpdateOne(context.Background(), filter, update, opts); err != nil {
		return fmt.Errorf("failed to store badge: %v", err)
	}
	return nil
}

func (d *Database) IsConnected() bool {
	if err := d.client.Ping(context.Background(), nil); err != nil {
		return false
	}
	return true
}
