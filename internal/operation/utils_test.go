package operation

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/zsoltimre/badger/internal/service/mongodb"
)

func TestUtils_getMongodbHandlerFromContext(t *testing.T) {
	assert := assert.New(t)
	db := &mongodb.Database{}
	ctx := &gin.Context{}
	ctx.Set("mongodb", db)
	result, err := getMongodbHandlerFromContext(ctx)
	assert.Nil(err)
	assert.True(db == result)
}
