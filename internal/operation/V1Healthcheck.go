package operation

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

func V1Healthcheck(c *gin.Context) {
	logger := log.With().
		Str("source", c.ClientIP()).
		Str("operation", "V1Healthcheck").
		Logger()
	mongo, err := getMongodbHandlerFromContext(c)
	if err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("error: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	connected := mongo.IsConnected()
	responseCode := http.StatusOK
	if !connected {
		responseCode = http.StatusInternalServerError
	}
	c.JSON(responseCode, connected)
}
