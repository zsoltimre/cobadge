package operation

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/zsoltimre/badger/internal/service/mongodb"
)

func getMongodbHandlerFromContext(c *gin.Context) (*mongodb.Database, error) {
	v, found := c.Get("mongodb")
	if !found {
		return nil, fmt.Errorf("MongoDB handler not found in service context")
	}
	mongo, success := v.(*mongodb.Database)
	if !success {
		return nil, fmt.Errorf("invalid MongoDB handler in service context")
	}
	return mongo, nil
}
