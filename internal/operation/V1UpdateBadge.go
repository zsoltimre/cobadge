package operation

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
	badge "gitlab.com/zsoltimre/badger/internal/badge"
)

type BadgeProperties struct {
	Key    string `json:"key" validate:"required,min=2"`
	Value  string `json:"value" validate:"required,min=1"`
	Status string `json:"status" validate:"oneof=neutral ok warning error"`
}

func V1UpdateBadge(c *gin.Context) {
	logger := log.With().
		Str("source", c.ClientIP()).
		Str("operation", "V1UpdateBadge").
		Logger()

	mongo, err := getMongodbHandlerFromContext(c)
	if err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("error: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	validate := validator.New()
	projectId := c.Param("projectId")
	if err := validate.Var(projectId, "required,uuid"); err != nil {
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid project ID in request: %v", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	branchName := c.Param("branchName")
	if err := validate.Var(branchName, "required,min=1,max=244"); err != nil {
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid branch name in request: %v", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	request := BadgeProperties{}
	if err := c.BindJSON(&request); err != nil {
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid request: %v", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	if err := validate.Struct(request); err != nil {
		validationErrors := err.(validator.ValidationErrors)
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid request: %s", validationErrors.Error())
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	badgeImage, err := badge.Create("badge", request.Key, request.Value, request.Status)
	if err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("unexpected error: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if err := mongo.UpdateBadge(projectId, branchName, request.Key, badgeImage); err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("unexpected error: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, true)
}
