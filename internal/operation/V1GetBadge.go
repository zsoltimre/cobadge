package operation

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

func V1GetBadge(c *gin.Context) {
	logger := log.With().
		Str("source", c.ClientIP()).
		Str("operation", "V1GetBadge").
		Logger()

	mongo, err := getMongodbHandlerFromContext(c)
	if err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("error: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	validate := validator.New()
	projectId := c.Param("projectId")
	if err := validate.Var(projectId, "required,uuid"); err != nil {
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid project ID in request: %v", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	branchName := c.Param("branchName")
	if err := validate.Var(branchName, "required,min=1,max=244"); err != nil {
		logger.Warn().
			Uint("response_code", http.StatusBadRequest).
			Msgf("invalid branch name in request: %v", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	key := c.Param("key")
	badge, err := mongo.GetBadge(projectId, branchName, key)
	if err != nil {
		logger.Error().
			Uint("response_code", http.StatusInternalServerError).
			Msgf("unexpected error: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if badge == nil {
		logger.Warn().
			Uint("response_code", http.StatusNotFound).
			Msgf("badge not found")
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	c.Data(200, "image/svg+xml", badge.Data)
}
