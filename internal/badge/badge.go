package badger

import (
	"bytes"
	"fmt"
	"strings"

	svg "github.com/ajstarks/svgo"
)

type StatusColorScheme map[string][]svg.Offcolor

func Create(title string, leftText string, rightText string, status string) ([]byte, error) {
	if len(leftText) == 0 {
		return []byte{}, fmt.Errorf("left panel text cannot be empty")
	}
	if len(rightText) == 0 {
		return []byte{}, fmt.Errorf("right panel text cannot be empty")
	}
	leftText = trimTextLength(leftText, 16)
	rightText = trimTextLength(rightText, 16)
	var result bytes.Buffer

	leftPanelWidth := getWidthByCharacterCount(len(leftText)) + 15
	rightPanelWidth := getWidthByCharacterCount(len(rightText)) + 20
	canvasWidth := leftPanelWidth + rightPanelWidth

	canvas := svg.New(&result)
	canvas.Start(canvasWidth, 20, "style=\"margin: 2px\"")
	canvas.Title(title)

	createCanvasColors(canvas, status)

	canvas.Group("fill=\"none\" fill-rule=\"evenodd\"")
	createLeftPanel(canvas, 0, leftPanelWidth, leftText)
	createRightPanel(canvas, leftPanelWidth, rightPanelWidth, rightText)
	canvas.Gend()

	canvas.End()
	byteValues := result.Bytes()
	lines := strings.Split(string(byteValues), "\n")
	return []byte(strings.Join(lines[2:], "\n")), nil
}

func createLeftPanel(canvas *svg.SVG, startPosition int, width int, text string) {
	canvas.Group("font-family=\"Verdana,Geneva,sans-serif\" font-size=\"11\"")
	canvas.Rect(startPosition, 0, width, 20, "fill=\"url(#left-fill)\" fill-rule=\"nonzero\" id=\"left-bg\"")
	canvas.Textspan(6, 15, "", "fill=\"#010101\" fill-opacity=\".3\"")
	canvas.Span(text, "x=\"6\" y=\"15\" aria-hidden=\"true\"")
	canvas.TextEnd()
	canvas.Textspan(5, 14, "", "fill=\"#FFFFFF\"")
	canvas.Span(text, "x=\"5\" y=\"14\"")
	canvas.TextEnd()
	canvas.Gend()
}

func createRightPanel(canvas *svg.SVG, startPosition int, width int, text string) {
	canvas.Group("transform=\"translate(" + getRightPanelStartX(startPosition) + ")\" font-family=\"Verdana,Geneva,sans-serif\" font-size=\"11\"")
	canvas.Rect(0, 0, width, 20, "fill=\"url(#right-fill)\" fill-rule=\"nonzero\" id=\"right-bg\"")
	canvas.Textspan(4, 15, "", "fill=\"#010101\" fill-opacity=\".3\" aria-hidden=\"true\"")
	canvas.Span(text, "x=\"4\" y=\"15\"")
	canvas.TextEnd()
	canvas.Textspan(4, 14, "", "fill=\"#FFFFFF\"")
	canvas.Span(text, "x=\"4\" y=\"14\"")
	canvas.TextEnd()
	canvas.Gend()
}

func createCanvasColors(canvas *svg.SVG, status string) {
	canvas.Def()
	canvas.LinearGradient("left-fill", 50, 0, 50, 100, []svg.Offcolor{
		{
			Offset:  0,
			Color:   "#444D56",
			Opacity: 1.00,
		},
		{
			Offset:  100,
			Color:   "#24292E",
			Opacity: 1.00,
		},
	})
	canvas.LinearGradient("right-fill", 50, 0, 50, 100, getStatusColor(status))
	canvas.DefEnd()
}

func getRightPanelStartX(start int) string {
	return fmt.Sprintf("%d", start)
}

func getWidthByCharacterCount(count int) int {
	return count * 7
}

func getStatusColor(status string) []svg.Offcolor {
	statusColor := StatusColorScheme{
		"neutral": []svg.Offcolor{
			{
				Offset:  0,
				Color:   "#222222",
				Opacity: 1.00,
			},
			{
				Offset:  100,
				Color:   "#222222",
				Opacity: 1.00,
			},
		},
		"ok": []svg.Offcolor{
			{
				Offset:  0,
				Color:   "#4caf50",
				Opacity: 1.00,
			},
			{
				Offset:  100,
				Color:   "#4caf50",
				Opacity: 1.00,
			},
		},
		"warning": []svg.Offcolor{
			{
				Offset:  0,
				Color:   "#ff9800",
				Opacity: 1.00,
			},
			{
				Offset:  100,
				Color:   "#ff9800",
				Opacity: 1.00,
			},
		},
		"error": []svg.Offcolor{
			{
				Offset:  0,
				Color:   "#f44336",
				Opacity: 1.00,
			},
			{
				Offset:  100,
				Color:   "#f44336",
				Opacity: 1.00,
			},
		},
	}
	color, found := statusColor[status]
	if !found {
		return statusColor["neutral"]
	}
	return color
}

func trimTextLength(value string, charCount uint) string {
	if uint(len(value)) <= charCount {
		return value
	}
	return value[0:charCount]
}
