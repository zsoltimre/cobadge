package badger

import (
	"bytes"
	"strings"
	"testing"

	svg "github.com/ajstarks/svgo"
	"github.com/stretchr/testify/assert"
)

func TestBadger_create(t *testing.T) {
	assert := assert.New(t)
	result, err := Create("Unittest Badge", "aaaaaaaaaaaaaaaa", "bbbbbbbbbbbbbbbb", "ok")
	assert.Nil(err)
	assert.NotEmpty(result)

	_, err = Create("Unittest Badge", "", "bbbbbbbbbbbbbbbb", "ok")
	assert.EqualError(err, "left panel text cannot be empty")
	_, err = Create("Unittest Badge", "aaaaaaaaaaaaaaaa", "", "ok")
	assert.EqualError(err, "right panel text cannot be empty")
}

func TestBadger_trimTextLength(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("aaaaaaaaaabbbbbb", trimTextLength("aaaaaaaaaabbbbbbbbbb", 16))
	assert.Equal("aaaaaaaaaabbbbbb", trimTextLength("aaaaaaaaaabbbbbb", 16))
	assert.Equal("aaaaaa", trimTextLength("aaaaaa", 10))
}

func TestBadger_getStatusColor(t *testing.T) {
	assert := assert.New(t)
	result := getStatusColor("unknown")
	assert.Equal("#222222", result[0].Color)
	assert.Equal("#222222", result[1].Color)
	result = getStatusColor("neutral")
	assert.Equal("#222222", result[0].Color)
	assert.Equal("#222222", result[1].Color)
	result = getStatusColor("ok")
	assert.Equal("#4caf50", result[0].Color)
	assert.Equal("#4caf50", result[1].Color)
	result = getStatusColor("warning")
	assert.Equal("#ff9800", result[0].Color)
	assert.Equal("#ff9800", result[1].Color)
	result = getStatusColor("error")
	assert.Equal("#f44336", result[0].Color)
	assert.Equal("#f44336", result[1].Color)
}

func TestBadger_getWidthByCharacterCount(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(70, getWidthByCharacterCount(10))
	assert.Equal(0, getWidthByCharacterCount(0))
}

func TestBadger_getRightPanelStartX(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("123", getRightPanelStartX(123))
}

func TestBadger_createCanvasColors(t *testing.T) {
	assert := assert.New(t)
	var result bytes.Buffer

	canvas := svg.New(&result)
	createCanvasColors(canvas, "ok")
	byteValues := result.Bytes()
	lines := strings.Split(string(byteValues), "\n")
	assert.Equal("<defs>", lines[0])
	assert.True(strings.HasPrefix(lines[1], "<linearGradient"))
	assert.True(strings.HasPrefix(lines[2], "<stop offset=\"0%\" stop-color=\"#444D56\""))
	assert.True(strings.HasPrefix(lines[3], "<stop offset=\"100%\" stop-color=\"#24292E\""))
	assert.True(strings.HasPrefix(lines[5], "<linearGradient"))
	assert.True(strings.HasPrefix(lines[6], "<stop offset=\"0%\" stop-color=\"#4caf50\""))
	assert.True(strings.HasPrefix(lines[7], "<stop offset=\"100%\" stop-color=\"#4caf50\""))
	assert.Equal("</defs>", lines[len(lines)-2])

	var result2 bytes.Buffer
	canvas = svg.New(&result2)
	createCanvasColors(canvas, "warning")
	byteValues = result2.Bytes()
	lines = strings.Split(string(byteValues), "\n")
	assert.Equal("<defs>", lines[0])
	assert.True(strings.HasPrefix(lines[1], "<linearGradient"))
	assert.True(strings.HasPrefix(lines[2], "<stop offset=\"0%\" stop-color=\"#444D56\""))
	assert.True(strings.HasPrefix(lines[3], "<stop offset=\"100%\" stop-color=\"#24292E\""))
	assert.True(strings.HasPrefix(lines[5], "<linearGradient"))
	assert.True(strings.HasPrefix(lines[6], "<stop offset=\"0%\" stop-color=\"#ff9800\""))
	assert.True(strings.HasPrefix(lines[7], "<stop offset=\"100%\" stop-color=\"#ff9800\""))
	assert.Equal("</defs>", lines[len(lines)-2])
}
