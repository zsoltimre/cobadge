package config

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"

	env "github.com/Netflix/go-env"
	"github.com/go-playground/validator/v10"
)

type Config struct {
	ListenAddress      string `env:"LISTEN_ADDRESS,default=0.0.0.0"`
	ListenPort         string `env:"LISTEN_PORT,default=8000"`
	MongodbUrl         string `env:"MONGODB_URL,required" validate:"required"`
	PostgresUrl        string `env:"POSTGRESQL_URL,required" validate:"required"`
	TlsCaCertPath      string `env:"TLS_CA_CERT_PATH,default=/tls/ca.crt" validate:"required"`
	TlsCertPath        string `env:"TLS_CERT_PATH,default=/tls/tls.crt" validate:"required"`
	TlsKeyPath         string `env:"TLS_KEY_PATH,default=/tls/tls.key" validate:"required"`
	TemporaryDirectory string `env:"TEMPORARY_DIRECTORY,default=/tmp" validate:"required"`
	UpdateToken        string `env:"UPDATE_TOKEN" validate:"required,min=32"`

	ServiceId  string
	Identity   string
	PrivateKey []byte
}

func Get() (*Config, error) {
	var config Config
	_, err := env.UnmarshalFromEnviron(&config)
	if err != nil {
		return nil, err
	}
	validate := validator.New(validator.WithRequiredStructEnabled())
	if err := validate.Struct(config); err != nil {
		return nil, err
	}
	key, err := os.ReadFile(config.TlsKeyPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read private key: %v", err)
	}
	config.PrivateKey = key
	identity, err := getCertificateSubjectCommonName(&config)
	if err != nil {
		return nil, err
	}
	config.Identity = identity
	return &config, nil
}

func getCertificateSubjectCommonName(c *Config) (string, error) {
	certificate, err := readCertificate(c.TlsCertPath)
	if err != nil {
		return "", err
	}
	return certificate.Subject.CommonName, nil
}

func readCertificate(certPath string) (*x509.Certificate, error) {
	cert, err := os.ReadFile(certPath) //#nosec G304 -- false positive
	if err != nil {
		return nil, fmt.Errorf("failed to read certificate: %v", err)
	}
	block, _ := pem.Decode(cert)
	if block == nil {
		return nil, fmt.Errorf("failed to decode certificate: %v", err)
	}
	certificate, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse certificate: %v", err)
	}
	return certificate, nil
}
